#!/bin/bash

export HEALTH=${CONDUCTOR_HEALTH:-http://conductor-server:8080/health}

echo 'Waiting for Conductor at ' $HEALTH ' ...' 

while [[ "$(curl -s -o /dev/null -L -w ''%{http_code}'' $HEALTH)" != 200 ]]; do
    echo 'still waiting ...'
    sleep 5
done

echo 'Starting provisioning workers...'

python3 PyExec.py Ansible Packer 
