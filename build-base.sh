#!/usr/bin/env bash
if [ $# -eq 0 ]
  then
    tag='latest'
  else
    tag=$1
fi

docker build -t nubisware/nubisware-conductor-worker-py-base:$tag -f docker/base/Dockerfile .
docker push nubisware/nubisware-conductor-worker-py-base:$tag
