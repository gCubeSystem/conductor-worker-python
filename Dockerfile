FROM python:3.7
COPY conductor/ /app/conductor
COPY pyexecplugins/ /app/pyexecplugins 
COPY requirements.txt PyExec.py config.cfg entrypoint.sh /app/
RUN pip install -r /app/requirements.txt
WORKDIR /app
ENTRYPOINT ["/app/entrypoint.sh"]
