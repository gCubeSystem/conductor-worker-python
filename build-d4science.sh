#!/usr/bin/env bash
if [ $# -eq 0 ]
  then
    tag='latest'
  else
    tag=$1
fi

docker build -t nubisware/nubisware-conductor-worker-py-d4s:$tag -f docker/d4science/Dockerfile .
docker push nubisware/nubisware-conductor-worker-py-d4s:$tag
