#!/bin/bash

export BASE="http://conductorserver:8080"

echo 'Waiting for Conductor ...'

while [[ "$(curl -s -o /dev/null -L -w ''%{http_code}'' $BASE/health)" != 200 ]]; do
    echo 'still waiting ...'
    sleep 5
done

echo 'Starting default workers...'

python3 PyExec.py Http Shell Eval Mail 
