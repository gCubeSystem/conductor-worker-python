from pyexecplugins.pyexecplugins import PyExecPlugin
import logging;

class Plugin(PyExecPlugin):
    name = "Eval"
    taskdef = {
        "name" : "pyeval",
        "description" : "Execute arbitrary python code",
        "inputKeys" : ["code"],
        "outputKeys" : ["result"],
        "ownerEmail" : "m.lettere@gmail.com"
    }

    def __init__(self, data=None, config=None):
        super().__init__(data, config)

    def execute(self):
        code = self.data.get("code")
        if code != None:
            logging.getLogger("pyexec").info("Going to eval: %s  | with data: %s", code, str(self.data))
            ret = eval(code, { "data" : self.data})
            return { "result" : ret } 
